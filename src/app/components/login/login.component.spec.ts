import { HttpClient } from "@angular/common/http";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { async,ComponentFixture, TestBed } from "@angular/core/testing";
import { BrowserDynamicTestingModule } from "@angular/platform-browser-dynamic/testing";
import { Router, RouterModule } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";
import { SocialAuthService, SocialLoginModule,SocialAuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider} from "angularx-social-login";
import { AuthServiceService } from "src/app/services/auth-service.service";
import { LoginComponent } from "./login.component";


describe('LoginComponent', ()=>{

    let component : LoginComponent;
    let fixture : ComponentFixture<LoginComponent>;
    let service : AuthServiceService;
    let router: Router;

   beforeEach(async (() => {
       TestBed.configureTestingModule({
        imports: [RouterTestingModule,SocialLoginModule,HttpClientTestingModule,BrowserDynamicTestingModule],
        declarations : [ LoginComponent ],
        providers: [
            {
              provide: 'SocialAuthServiceConfig',
              useValue: {
                autoLogin: true,
                providers: [
                  {
                    id: GoogleLoginProvider.PROVIDER_ID,
                    provider: new GoogleLoginProvider(
                      'clientId'
                    )
                  },
                  {
                    id: FacebookLoginProvider.PROVIDER_ID,
                    provider: new FacebookLoginProvider('clientId')
                  }
                ],
                onError: (err) => {
                  console.error(err);
                }
              } as SocialAuthServiceConfig,
            },
            
          ],

       }).compileComponents();
   }))

   beforeEach(()=>{
    fixture = TestBed.createComponent(LoginComponent);
    router = TestBed.get(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
   });

   it('should create Login Component',()=>{
       expect(component).toBeTruthy();
   })
   
});  